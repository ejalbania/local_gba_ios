//
//  AddRecipientViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class AddRecipientViewController: ManagePayeesModuleViewController{
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    @IBOutlet weak var countryCode_textField: GBATitledTextField!
    
    private var selectedCountry: Countries = .Philippines
    
    private var currentPresenter: AddRecipientPresenter{
        get{
            guard let prsntr = self.presenter as? AddRecipientPresenter else{
                fatalError("Error in parsing AddRecipientViewController")
            }
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.set(view: self)
        let defaultCountry = Countries.Philippines
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(doneBtn_tapped))
        
        self.country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
        
        self.countryCode_textField
            .set(self)
            .set(text: "+63")
            .set(textFieldIsEditable: false)
            .set(returnKey: .next)
        
        self.mobileNumber_textField
            .set(self)
            .set(placeholder: "Mobile Number")
            .set(keyboardType: .phonePad)
            .set(returnKey: .done)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Add Recipient"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = " "
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc private func doneBtn_tapped(){
        self.view.resignFirstResponder()
        
        if mobileNumber_textField.text != "" {
            let mobileNumber = "\(countryCode_textField.text)\(mobileNumber_textField.text)"
            self.currentPresenter.checkPayee(mobile: mobileNumber, successHandler: {
                //di ko alam ilalagay dito -_-
            })
        }else{
            self.mobileNumber_textField
                .set(required: true)
            
        }
    }
}

extension AddRecipientViewController: GBAFocusableInputViewDelegate{
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
}

extension AddRecipientViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        
        self.countryCode_textField
            .set(text: cell.countryCode)
            .set(placeholder: "")
    }
}

extension AddRecipientViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

