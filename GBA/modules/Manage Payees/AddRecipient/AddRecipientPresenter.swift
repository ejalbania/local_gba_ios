//
//  AddRecipientPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class AddRecipientPresenter: ManagePayeesRootPresenter{
    
    func checkPayee(mobile: String, successHandler: @escaping ()->Void){
        self.interactor.remote.CheckPayee(with: mobile) { (reply, statusCode) in
            print("STATUSCODE ", statusCode)
            print("REPLY ", reply)
            
            switch statusCode{
            case .fetchSuccess:
                guard let inPayee = reply["in_payee"] as? Int else {
                    fatalError("Required in_payee value not found in AddRecipientPresenter")
                }
                
                if inPayee == 0{ self.viewPayeeForAdding(payee: Payee(json: reply)) }
                else{ self.presentToast(title: "Warning", message: "Person with this mobile number is already added to your payee") }
                
            case .conflict:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
                let sameMobileNumber = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
                }
                
                sameMobileNumber.addAction(confirmAction)
                
                self.view.present(sameMobileNumber, animated: true, completion: nil)
            case .notFound:
                //show alert telling the user to send invite
                let alert = UIAlertController(title: "Invite", message: "This contact number does not have a GBA Account yet. Would you like to invite \(String(describing: mobile))?", preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                    print("Cancel")
                }
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                    //API connect -> send invite to user
                    
                    self.interactor.remote.InvitePayee(with: mobile, successHandler: { (reply, statusCode) in
                        
                        switch statusCode{
                        case .fetchSuccess:
                            let successfulInvite = UIAlertController(title: "Success", message: "Invited successfully", preferredStyle: .alert)
                            
                            let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                //redirect to popViewController
                                self.wireframe.popToRootViewController(true)
                            }
                            
                            successfulInvite.addAction(confirmAction)
                            
                            self.view.present(successfulInvite, animated: true, completion: nil)
                        default: break
                        }
                    })
                }
                
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                
                self.view.present(alert, animated: true, completion: nil)
            case .badRequest:
                guard let message = reply["message"] as? [String:Any],
                    let error = (message["mobile"] as? [String])?.first else {
                         return
                }
                self.showAlert(with: "Warning", message: error, completion: { } )
                
            default: break
            }
        }
    }
    
    func viewPayeeForAdding(payee: Payee){
        self.wireframe.navigate(to: .EditPayeescreen(.add,payee))
    }

    private func presentToast(title: String?, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
    }
}

