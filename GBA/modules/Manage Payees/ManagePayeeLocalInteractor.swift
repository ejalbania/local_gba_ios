//
//  ManagePayeeLocalInteractor.swift
//  GBA
//
//  Created by Republisys on 23/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import RealmSwift

class ManagePayeeLocalInteractor:RootLocalInteractor{
    
    var PayeeList: [Payee]?{
        get{ return self.fetchList(entity: Payee.self) }
    }

}
