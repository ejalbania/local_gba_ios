//
//  ManagePayeesCellView.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class PayeesCellView: UITableViewCell {

    @IBOutlet private weak var caret_label: UILabel!
    @IBOutlet private weak var avatar_icon: UIImageView!
    @IBOutlet private weak var payeeName_label: UILabel!
    @IBOutlet private weak var mobile_label: UILabel!
    
    var payee: Payee?{
        didSet{
            guard let _payee = self.payee else{
                return
            }
            
            let displayName = _payee.nickname == "" ? _payee.fullname: _payee.nickname
            let mobile = _payee.mobile
            
            self.payeeName = displayName ?? ""
            self.mobile = mobile ?? ""
        }
    }
    
    var payeeName: String{
        get{ return payeeName_label.text ?? "" }
        set{ self.payeeName_label.text = newValue }
    }
    var mobile: String{
        get{ return mobile_label.text ?? "" }
        set{ self.mobile_label.text = newValue }
    }
    
    var uid: Int?{
        get{
            guard let _payee = self.payee else{ return nil }
            return _payee.uid
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        caret_label.font = UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)
        caret_label.text = String.fontAwesomeIcon(name: .angleRight)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.avatar_icon.layer.cornerRadius = 23
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
}

extension PayeesCellView{
    @discardableResult
    func set(payee: Payee)->Self{
        self.payee = payee
        return self
    }
    
    
}

