//
//  EditPayeeModeEnum.swift
//  GBA
//
//  Created by Republisys on 30/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

enum EditPayeeMode: String{
    case add = "Add", update = "Edit" , read = "View"
}
