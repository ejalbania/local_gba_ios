//
//  EditPayeePresenter.swift
//  GBA
//
//  Created by Republisys on 30/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class EditPayeePresenter: ManagePayeesRootPresenter{
    
    var mode: EditPayeeMode = .read
    fileprivate var payee: Payee? = nil
    
    
    func addRecipient(payee: Payee){
        self.interactor.remote.addPayee(id: payee.uid.StringValue, nickname: payee.nickname!) { (reply, replyCode) in
            print(reply)
            print(replyCode)
            switch replyCode{
            case .dataCreated:
                guard let message = reply["message"] as? String else{
                    self.presentToast(title: "Not Found", message: "Process was successful but the message was not found.")
                    return
                }
                self.presentToast(title: "Success", message: message)
            default: break
            }
        }
    }
    
    func getPayee()->Payee?{
        return payee
    }
    
    private func presentToast(title: String?, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (_) in
            alert.dismiss(animated: true, completion: {
                self.view.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
}

extension EditPayeePresenter{
    @discardableResult
    func set(mode: EditPayeeMode)->Self{
        self.mode = mode
        return self
    }
    
    @discardableResult
    func set(payee: Payee?)->Self{
        self.payee = payee
        return self
    }
}
