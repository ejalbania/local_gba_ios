//
//  EditPayeeViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/4/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class EditPayeeViewController: ManagePayeesModuleViewController{
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var nickName_textField: GBATitledTextField!
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    private var selectedCountry: Countries = .Philippines
    
    var currentPresenter: EditPayeePresenter{
        guard let prsntr = self.presenter as? EditPayeePresenter else{
            fatalError("Error in parsing presenter in EditPayeeViewController0")
        }
        return prsntr
    }
    
    private var payee: Payee{
        get{ return self.currentPresenter.getPayee() ?? Payee() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.set(view: self)
        
        let defaultCountry = Countries.Philippines
        
        firstName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
        
        lastName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Last name")
            .set(next: nickName_textField)
        
        nickName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(titledFieldDelegate: self)
            .set(placeholder: "Nickname")
            .set(next: mobileNumber_textField)
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))

        mobileNumber_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
        
        email_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
        
        self.setEditMode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "\(self.currentPresenter.mode.rawValue) PAyee"
        
        self.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    internal func setEditMode(){
        switch self.currentPresenter.mode {
        case .update:
            break
        case .add:
            self.addSubmitButton(title: "Add")
            self.firstName_textField.set(userInteractionEnabled: false)
            self.lastName_textField.set(userInteractionEnabled: false)
            self.nickName_textField.set(userInteractionEnabled: true)
            self.country_dropDown.isUserInteractionEnabled = false
            self.mobileNumber_textField.set(userInteractionEnabled: false)
            self.email_textField.set(userInteractionEnabled: false)
        case .read:
            self.firstName_textField.set(userInteractionEnabled: false)
            self.lastName_textField.set(userInteractionEnabled: false)
            self.nickName_textField.set(userInteractionEnabled: false)
            self.country_dropDown.isUserInteractionEnabled = false
            self.mobileNumber_textField.set(userInteractionEnabled: false)
            self.email_textField.set(userInteractionEnabled: false)
        }
    }
    
    internal func reloadData(){
        let payee = self.payee
        self.firstName_textField.set(text: payee.firstname!)
        self.lastName_textField.set(text: payee.lastname!)
        self.nickName_textField.set(text: payee.nickname ?? "")
        self.nickname_label.text = (nickname_label.text != "") ? nickname_label.text: "\(firstName_textField.text) \(lastName_textField.text)"
        self.selectedCountry = Countries(country: payee.country_id ?? "PH")
        self.mobileNumber_textField.set(text: payee.mobile!)
        self.email_textField.set(text: payee.email!)


    }
    
    private func addSubmitButton(title: String){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: title,
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submitBtn))
    }
    
    @objc private func submitBtn(){
        switch self.currentPresenter.mode{
        case .add:
            let payee = self.payee
            payee.nickname = self.nickName_textField.text
            self.currentPresenter.addRecipient(payee: payee)
        case .update:
            break
        case .read:
            break
        }
    }
    
    override func backBtn_tapped(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension EditPayeeViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func didChange(value: String?) {
        self.nickname_label.text = value
    }
}

extension EditPayeeViewController:GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        if textField == self.nickName_textField{
            self.nickname_label.text = (textField.text != "") ? textField.text: "\(firstName_textField.text) \(lastName_textField.text)"
        }
    }
}

extension EditPayeeViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditPayeeViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}
