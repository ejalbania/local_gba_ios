//
//  ManagePayeeRemoteInteractor.swift
//  GBA
//
//  Created by Republisys on 22/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "/public" } }
    
    case fetchPayee
    case checkPayee(mobile: String)
    case invitePayee(mobile: String)
    case addPayee(id: String, nickname: String)
    
    var route: Route{
        switch self {
        case .fetchPayee:
            return Route(   method              : .get,
                            suffix              : "/payees",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .checkPayee(let mobile):
            return Route(   method              : .post,
                            suffix              : "/payee/check",
                            parameters          :["mobile" : mobile],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .invitePayee(let mobile):
            return Route(   method              : .post,
                            suffix              : "/invite/user",
                            parameters          : ["mobile" : mobile],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .addPayee(let id,let nickname):
            return Route(method                 : .post,
                         suffix                 : "payees",
                         parameters             : ["user_id"    : id,
                                                   "nickname"   : nickname],
                         waitUntilFinished      : true,
                         nonToken               : false)
        }
    }
    
}

class ManagePayeeRemoteInteractor: RootRemoteInteractor{
    
    func FetchPayeeList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.fetchPayee, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    func CheckPayee(with mobile: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.checkPayee(mobile: mobile), successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
    
    func InvitePayee(with mobile: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.invitePayee(mobile: mobile), successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
    
    func addPayee(id: String, nickname: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.addPayee(id: id, nickname: nickname), successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
}
