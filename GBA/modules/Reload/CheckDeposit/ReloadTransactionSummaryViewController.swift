//
//  ReloadTransactionSummaryViewController.swift
//  GBA
//
//  Created by Gladys Prado on 16/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class ReloadTransactionSummaryViewController: ReloadRootViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.parent?.title = "Reload Summary"
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        let amount = NSAttributedString(string: "$250.00",
                                        attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        let message = NSAttributedString(string: " was successfully deposited into your ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
        let destinationAccount = NSAttributedString(string: "USD Account",
                                                    attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        
        let content = NSMutableAttributedString()
        content.append(amount)
        content.append(message)
        content.append(destinationAccount)
        
        self.presenter.wireframe.presentSuccessPage(title: "Check Deposit", message: content, doneAction: {
            
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
        
    }
}
