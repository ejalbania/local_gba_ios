//
//  PhotoUploadViewController.swift
//  GBA
//
//  Created by Gladys Prado on 16/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PhotoUploadViewController: ReloadRootViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    enum Constants{
        static let Camera = "Camera"
        static let Upload = "Upload"
    }
    
    @IBOutlet weak var imgFrontPartHolder: UIImageView!
    @IBOutlet weak var imgBackPartHolder: UIImageView!
    @IBOutlet weak var viewFront: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var typeOfImage: String = ""
    var img = UIImageView ()
    var isApprove: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewFront.isUserInteractionEnabled = true
        let frontImageTap = UITapGestureRecognizer(target: self, action: #selector(showActionSheetForPhoto))
        viewFront.gestureRecognizers = [frontImageTap]
        
        self.viewBack.isUserInteractionEnabled = true
        let backImageTap = UITapGestureRecognizer(target: self, action: #selector(showActionSheetForPhoto))
        viewBack.gestureRecognizers = [backImageTap]
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.navigationItem.leftBarButtonItem? = UIBarButtonItem(title: "Cancel",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempCancelButton))
        self.navigationItem.leftBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.viewFront.layer.borderWidth = 1
        self.viewFront.layer.borderColor = GBAColor.midGray.rawValue.cgColor
        self.viewFront.layer.cornerRadius = 5
        
        self.viewBack.layer.borderWidth = 1
        self.viewBack.layer.borderColor = GBAColor.midGray.rawValue.cgColor
        self.viewBack.layer.cornerRadius = 5
        
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Reload"
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @IBAction func submit(sender: UIButton) {
        if (imgFrontPartHolder.image == nil) && (imgBackPartHolder.image == nil) {
            // Create the alert controller
            let alertController = UIAlertController(title: "Upload", message: "Please click below to upload.", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            // Create the alert controller
            let alertController = UIAlertController(title: "Upload", message: "Are you sure you want to upload this photo?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                //call for function uploading image to server goes here
//                self.uploadWithAlamofire()
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func btnUpload(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            if status == .authorized {
                // authorized
                isApprove = true
                showActionSheetForPhoto()
                print("authorized")
            }
            else if status == .denied {
                // denied
                isApprove = false
                showActionSheetForPhoto()
                print("denied")
            }
            else if status == .restricted {
                // restricted
                print("restricted")
            }
            else if status == .notDetermined {
                // not determined
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {(_ granted: Bool) -> Void in
                    if granted {
                        print("Granted access")
                    }
                    else {
                        print("Not granted access")
                    }
                })
            }
        }
    }
    
    
    @objc func showActionSheetForPhoto(){
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takeAPhoto = UIAlertAction(title: "Take a Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                if self.isApprove{
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                    imagePicker.allowsEditing = false
                    self.typeOfImage = Constants.Camera
                    self.present(imagePicker, animated: true, completion: nil)
                    
                }else{
                    self.alertPromptToAllowCameraAccessViaSettings()
                }
            }
            
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                self.typeOfImage = Constants.Upload
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(takeAPhoto)
        optionMenu.addAction(gallery)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img = UIImageView(image: image)
            switch typeOfImage {
            case Constants.Camera:
                savePhoto()
                imgFrontPartHolder.image = image
            default:
                imgFrontPartHolder.image = image
                picker.dismiss(animated: true, completion: nil)
                break
            }
            viewFront.alpha = 0
            imgFrontPartHolder.alpha = 1
            imgFrontPartHolder.layer.masksToBounds = true
            imgFrontPartHolder.layer.borderColor = GBAColor.darkGray.rawValue.cgColor
            imgFrontPartHolder.layer.cornerRadius = 5
            imgFrontPartHolder.layer.borderWidth = 2
            
            
        } else{
            print("Something went wrong")
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
    }
    
    
    func savePhoto(){
        
        let imageData = UIImageJPEGRepresentation(img.image!, 0.6)
        let compressedJPGImage = UIImage(data: imageData!)
        UIImageWriteToSavedPhotosAlbum(compressedJPGImage!, nil, nil, nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSettings() {
        let alert = UIAlertController(title: "Global Bank Access would like to access your camera", message: "", preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) { alert in
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
            }
        })
        present(alert, animated: true, completion: nil)
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        ReloadWireframe(nav).navigate(to: .ReloadAmountDepositedView)
    }
    
    @objc override func tempCancelButton() {
        self.presenter.wireframe.popFromViewController(true)
    }
    
}


