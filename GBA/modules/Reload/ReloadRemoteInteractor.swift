
//
//  ReloadRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 18/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum ReloadAPICalls: Router{
    
    var baseComponent: String { get { return "public" } }
    
    case directDebitDeposit(wallet_id: String, amount: String, account_name: String, account_number: String, aba_number: String)
    
    var route: Route {
        switch self {
        case .directDebitDeposit(let wallet_id, let amount, let account_name, let account_number, let aba_number):
            return Route(method: .post,
                         suffix: "/transactions/reloads/debit-deposit",
                         parameters:
                [ "wallet_id"               : wallet_id,
                  "amount"                  : amount,
                  "account_name"            : account_name,
                  "account_number"          : account_number,
                  "aba_number"              : aba_number],
                         waitUntilFinished  : true,
                         nonToken           : false)
        }
    }
}

class ReloadRemoteInteractor: RootRemoteInteractor{
    private var fieldDirectDepositEntity: DirectDepositEntity? = nil
    private var fieldCreditCardReloadEntity: CreditCardReloadEntity? = nil
    
    //Direct Debit Deposit Reload
    func DirectDebitDeposit(form: DirectDepositEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        fieldDirectDepositEntity = form
        
        NetworkingManager.request(ReloadAPICalls.directDebitDeposit(wallet_id: form.wallet_id!,
                                                              amount: form.amount!,
                                                              account_name: form.account_name!,
                                                              account_number:form.account_number!,
                                                              aba_number: form.aba_number!), successHandler: {
                                                                (reply, statusCode) in
                                                                successHandler(reply, statusCode)
                                                                
        })
    }
    
}
