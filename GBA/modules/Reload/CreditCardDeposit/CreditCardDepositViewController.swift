//
//  CreditCardDepositViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

class CreditCardDepositViewController: ReloadRootViewController, GBAFocusableInputViewDelegate {
    
    @IBOutlet weak var txtCardName: GBATitledTextField!
    @IBOutlet weak var txtCardNumber: GBATitledTextField!
    @IBOutlet weak var txtCardExpirationDate: GBATitledTextField!
    @IBOutlet weak var txtCardCVV: GBATitledTextField!
    @IBOutlet weak var txtCardDepositAmount: GBATitledTextField!

    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    var directDebitDepositForm: DirectDepositEntity{
        return DirectDepositEntity( wallet_id: "\(self.wallet!.id)",
            amount: self.txtCardDepositAmount.text,
            account_name: self.txtCardName.text,
            account_number: self.txtCardNumber.text,
            aba_number: self.txtCardCVV.text)
    }
    
    var currentPresenter: CreditCardDepositPresenter{
        guard let prsntr = self.presenter as? CreditCardDepositPresenter
            else{ fatalError("Error in parsing presenter for DirectDebitAccountDepositViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        print("self.presenter ", self.presenter)
        super.viewDidLoad()
        self.presenter.set(view: self)
        
        self.txtCardName
            .set(self)
            .set(placeholder: "Card Name")
            .set(returnKey: .next)
            .set(next: txtCardCVV)
        
        self.txtCardNumber
            .set(self)
            .set(placeholder: "Card Number")
            .set(keyboardType: .numberPad)
            .set(returnKey: .next)
            .set(next: txtCardExpirationDate)
        
        self.txtCardExpirationDate
            .set(self)
            .set(placeholder: "Expiration (MM/YY)")
            .set(keyboardType: .numberPad)
            .set(inputType: .expiryDate)
            .set(returnKey: .next)
            .set(next: txtCardCVV)
        
        self.txtCardCVV
            .set(self)
            .set(placeholder: "CVV")
            .set(keyboardType: .numberPad)
            .set(inputType: .cardCVV)
            .set(security: true)
            .set(returnKey: .next)
            .set(next: txtCardDepositAmount)
        
        self.txtCardDepositAmount
            .set(self)
            .set(placeholder: "Amount")
            .set(keyboardType: .decimalPad)
            .set(returnKey: .done)
        
        let cancelButton = UIBarButtonItem(title: "Cancel",
                                           style: .plain,
                                           target: self,
                                           action: #selector(tempCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.leftItemsSupplementBackButton = true
        
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "Reload"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
    }
    
    @objc override func tempCancelButton() {
        self.presenter.wireframe.popFromViewController(true)
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.view.subviews.forEach{ $0.resignFirstResponder() }
        
        if (self.txtCardName.text != "" && self.txtCardNumber.text != "" && self.txtCardCVV.text != "" && self.txtCardDepositAmount.text != "" && isCardExpired(self.txtCardExpirationDate.text) == false) {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.currentPresenter.submitDirectDebitDeposit(directDebitDepositReloadForm: directDebitDepositForm)
        } else {
            self.fieldValidations()
        }
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func fieldValidations(){
        self.txtCardName
            .set(required: true)
            .set(regex: [GBAValidations.alphaNumeric.stringValue], validationMessage:["Invalid account name"])
        
        self.txtCardCVV
            .set(required: true)
        
        self.txtCardExpirationDate
            .set(required: true)
        
        self.txtCardNumber
            .set(required: true)
        
        self.txtCardDepositAmount
            .set(required: true)
        
    }
    
    func isCardExpired(_ expiryDate: String) -> Bool {
        let dateNow = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/YY"

        let currentDateConversion = dateFormatter.string(from: dateNow)
        let currentDate = dateFormatter.date(from: currentDateConversion)
        let selectedDate = dateFormatter.date(from: expiryDate)
        
        if let _ = dateFormatter.date(from: expiryDate){
            if currentDate?.compare(selectedDate!) == .orderedDescending {
                print("Card expired")
//                let alert = UIAlertController(title: "Notice", message: "Card is already expired.", preferredStyle: .alert)
//
//                self.present(alert, animated: true, completion: nil)
                
                let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: "Card is already expired.", preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                unauthorizedAccessAlert.addAction(confirmAction)
                
                self.present(unauthorizedAccessAlert, animated: true, completion: nil)
                
                return true
            } else {
                return false
            }
        }else{
//            let alert = UIAlertController(title: "Notice", message: "Please enter a valid date.", preferredStyle: .alert)
//
//            self.present(alert, animated: true, completion: nil)
            
            let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: "Please enter a valid date.", preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                //dismiss alert
            }
            
            unauthorizedAccessAlert.addAction(confirmAction)
            
            self.present(unauthorizedAccessAlert, animated: true, completion: nil)
            
            return true
        }
    }
    
    
}

