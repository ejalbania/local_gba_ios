//
//  DashboardViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class DashboardViewController: DashboardModuleViewController{
    @IBOutlet weak var reloadControl_container: UIView!
    @IBOutlet weak var reload_btn: UIButton!
    @IBOutlet weak var clickableArea: UIView!
    @IBOutlet weak var balance_label: UILabel!
    
    @IBOutlet weak var cardAspectRatio: NSLayoutConstraint!
    @IBOutlet weak var cardImageView_contrainer: GBACard!
    @IBOutlet weak var pullCardDrawer_btn: UIButton!
    
    @IBOutlet weak var transactionHistory_tableView: UITableView!
    
    private var cardIsShown: Bool = true
    
    private var currentDate = Date.Now
    private var cells01 = [
        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView
    ]
    
    private var cells02 = [
        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView,        Bundle.main.loadNibNamed("DashboardTransactionHistoryCellView", owner: self, options: nil)?.first as! DashboardTransactionHistoryCellView
    ]

    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    fileprivate var profile: User?{
        get{ return self.presenter.interactor.local.userProfile }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let notifIcon = UIBarButtonItem(image: UIImage(ciImage: CIImage(image: UIImage(named: "Notification")!)!,
                                                       scale: 12,
                                                       orientation: .up),
                                        style: .plain,
                                        target: self,
                                        action: nil)
        
        
        self.parent?.navigationItem.rightBarButtonItem = notifIcon
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.reload_btn.layer.borderWidth = 1
        self.reload_btn.layer.borderColor = GBAColor.white.rawValue.cgColor
        self.reload_btn.layer.cornerRadius = 3
        
        self.balance_label.font = GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue
        
        self.pullCardDrawer_btn.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
        self.pullCardDrawer_btn.setTitle(String.fontAwesomeIcon(name: .angleUp), for: .normal)
        
        self.transactionHistory_tableView.delegate = self
        self.transactionHistory_tableView.dataSource = self
        
        self.transactionHistory_tableView.backgroundColor = GBAColor.lightGray.rawValue
        
        let reloadContainer_tapped = UITapGestureRecognizer(target: self, action: #selector(toggleCardDrawer_tapped(_:)))
        
        self.clickableArea.isUserInteractionEnabled = true
        self.clickableArea.addGestureRecognizer(reloadContainer_tapped)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Dashboard"
        
        self.presenter.fetchUserInfo {
            self.presenter.fetchWallet {
                self.repopulateCardInfo()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.parent?.title = " "
        self.parent?.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func toggleCardDrawer_tapped(_ sender: Any) {
        guard let ratio = self.cardAspectRatio else { return }
        self.cardAspectRatio.Disable()
        
        switch self.cardIsShown {
        case true:
            UIView.animate(withDuration: 1) {
                self.cardAspectRatio = ratio.firstItem?.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.01).Enable()
                self.cardImageView_contrainer.alpha = 0
                self.pullCardDrawer_btn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.view.layoutIfNeeded()
            }
        case false:
            self.cardImageView_contrainer.set(isAnimating: true)
            UIView.animate(withDuration: 0.5, animations: {
                self.pullCardDrawer_btn.transform = CGAffineTransform(rotationAngle: 0)
                self.cardAspectRatio = ratio.firstItem!.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 10/45).Enable()
                self.cardImageView_contrainer.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: { _ in
                UIView.animate(withDuration: 0.5){
                    self.cardImageView_contrainer.set(isAnimating: false)
                    self.view.layoutIfNeeded()
                }
            })
            
        }
        
        self.cardIsShown = !self.cardIsShown
    }
    @IBAction func reloadBtn_tapped(_ sender: UIButton) {
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = false
        
        ReloadWireframe(_nav).navigate(to: .ReloadOptionsView)
        self.present(_nav, animated: true, completion: nil)
    }
    
    private func repopulateCardInfo(){
        guard let userProfile = self.profile,
            let userWallet = self.wallet else{
            return
        }
        self.cardImageView_contrainer
            .set(owner: userProfile.Fullname)
        
        self.balance_label.text = "\(userWallet.currency) \(userWallet.balance.toString)"
        self.cardImageView_contrainer.layoutSubviews()
    }
}

extension DashboardViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var dateString = ""
        let sectionView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 40)))
        sectionView.backgroundColor = GBAColor.lightGray.rawValue
        
        guard let day = currentDate.day,
            let month = currentDate.monthAbv,
            let year = currentDate.year
            else{ fatalError("Problem in parsing date in dashboard viewController") }
        
        if section == 0{
            dateString = "TODAY \(day) \(month.uppercased()) \(year)"
        }else{
            dateString = "YESTERDAY \(day - 1) \(month.uppercased()) \(year)"
        }
        
        let date = UILabel()
            .set(color: GBAColor.darkGray.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(12).rawValue)
            .add(to: sectionView)
            .set(value: dateString)

        date.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10).Enable()
        date.widthAnchor.constraint(equalTo: sectionView.widthAnchor, multiplier: 9/10).Enable()
        date.centerXAnchor.constraint(equalTo: sectionView.centerXAnchor).Enable()
        date.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor).Enable()

        return sectionView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        let cells = indexPath.section == 0 ? cells01 : cells02
        
        let cell = cells[index]
        if index == 0{
            cell.set(name: "sample text here boom!")
            cell.position = .top
        }else if index > 0 && index < cells.count - 1{
            cell.position = .middle
        }else if index == cells.count - 1{
            cell.position = .bottom
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}

extension DashboardViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? cells01.count: cells02.count
    }
}



