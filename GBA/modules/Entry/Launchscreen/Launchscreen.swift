//
//  ViewController.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/20/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit

class Launchscreen: EntryModuleViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let appNav = self.navigationController else {
            fatalError("App navigator not found!")
        }
        appNav.isNavigationBarHidden = true
        
        let image = UIImageView()
        image.image = UIImage(imageLiteralResourceName: "logo_white")
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        
        self.view.addSubview(image)
        
        image.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).Enable()
        image.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).Enable()
        image.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/2).Enable()
        image.heightAnchor.constraint(equalTo: image.widthAnchor).Enable()
        
        view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let appNav = self.navigationController!
        
        print(UserDefaults.standard.bool(forKey: "isFirstOpened"))
        
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: {
                _ in
                if UserDefaults.standard.bool(forKey: "isNotFirstOpened"){ EntryWireframe(appNav).navigate(to: .Loginscreen) }
                else{ EntryWireframe(appNav).navigate(to: .Welcomescreen) }
            })
        } else {
            
        }
    }
}

