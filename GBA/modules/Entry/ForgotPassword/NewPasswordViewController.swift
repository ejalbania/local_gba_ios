//
//  NewPasswordViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class NewPasswordViewController: EntryModuleViewController{
    
    @IBOutlet weak var newPassword_textField: GBATitledTextField!
    @IBOutlet weak var confirmPassword_textField: GBATitledTextField!
    
    private var currentPresenter: ForgotPasswordPresenter{
        guard let prsntr = self.presenter as? ForgotPasswordPresenter else {
            fatalError("Presenter was not properly set in ForgotPasswordViewController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        self.addBackButton()
        self.view.backgroundColor = .white
        self.title = "New Password"
        
        self.currentPresenter.set(view: self)
        
        newPassword_textField
            .set(placeholder: "New Password")
            .set(security: true)
            .set(self)
        
        confirmPassword_textField
            .set(placeholder: "Confirm Password")
            .set(security: true)
            .set(self)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submit_tapped(_:)))
    }
    
    @IBAction func submit_tapped(_ sender: GBAButton) {
        let newPassword = self.newPassword_textField.text
        let confirmNewPassword = self.confirmPassword_textField.text
        
        self.currentPresenter.sendNewPassword(newPassword: newPassword, confirm_password: confirmNewPassword)
        
//      self.presenter.wireframe.presentSuccessPage(title: "SUCCESS", message: NSAttributedString(string: "Password was successfully updated"))
    }
    
    @objc override func backBtn_tapped(){
        
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel changing your password", preferredStyle: .alert)
        let yes = UIAlertAction(title: "YES", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
        
    }
}

