//
//  ForgotPasswordViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/19/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: EntryModuleViewController{
    
    @IBOutlet weak var  mobileNumber_textField: GBATextField!
    
    override func viewDidLoad() {
        self.addBackButton()
        
        self.view.backgroundColor = .white
        
        self.mobileNumber_textField
            .set(self)
            .set(alignment: .center)
            .set(inputType: .mobileNumber)
            .set(placeholder: "Mobile Number")
            .set(text: "+639123456001")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submit_tapped(_:)))
    }
    
    private var currentPresenter: ForgotPasswordPresenter{
        guard let prsntr = self.presenter as? ForgotPasswordPresenter else {
            fatalError("Presenter was not properly set in ForgotPasswordViewController")
        }
        return prsntr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.set(view: self)
        
        self.title = "Forgot Password"
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }
    
    @IBAction func submit_tapped(_ sender: GBAButton) {
        self.currentPresenter.sendVerificationCode(to: mobileNumber_textField.text)
    }
    
    override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
}

extension ForgotPasswordViewController: GBAVerificationCodeDelegate{
    func ResendButton_tapped(sender: UIButton) {
        self.currentPresenter.resendVerificationCode {
            var countdown = 60
            
            sender.isEnabled = false
            sender.setTitleColor(GBAColor.darkGray.rawValue, for: .disabled)
            
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                sender.setTitle("RESEND CODE (\(countdown))", for: .disabled)

                let _ = countdown-- < 0 ? (timer.invalidate(), (sender.isEnabled = true)): ((),())
            })
        }
    }
    
    func GBAVerification() {
        self.presenter.wireframe.navigate(to: .NewPasswordscreen)
    }
}
