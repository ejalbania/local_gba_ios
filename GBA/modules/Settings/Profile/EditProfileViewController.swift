//
//  EditProfileViewController.swift
//  GBA
//
//  Created by Gladys Prado on 18/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class EditProfileViewController: SettingsRootViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    private var selectedCountry: Countries = .Philippines
    
    //EXP.
    fileprivate var profile: User?{
        get{
            return self.presenter.interactor.local.userProfile
        }
    }
    
    var editProfileForm: EditProfile{
        return EditProfile(firstName: self.firstName_textField.text, lastName: self.lastName_textField.text)
    }
    
    var currentPresenter: EditProfilePresenter{
        guard let prsntr = self.presenter as? EditProfilePresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
        
    override func viewDidLoad() {
        print("*** ViewDidLoad ***")
        
        self.presenter.set(view: self)
        (self._presenter as! EditProfilePresenter).dataBridge = self
        
        let defaultCountry = Countries.Philippines
        
        //profile_imageView
        self.profile_imageView.layer.cornerRadius = profile_imageView.frame.size.width/2
        self.profile_imageView.clipsToBounds = true
        self.profile_imageView.layer.borderWidth = 3
        self.profile_imageView.layer.borderColor = UIColor.white.cgColor
        
        firstName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
        
        lastName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Last name")
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
        
        mobileNumber_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
        
        email_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("*** ViewDidAppear ***")
        super.viewDidAppear(animated)
        self.title = "Edit Payee"
        self.repopulateProfileInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("*** ViewWillAppear ***")
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("*** ViewWillDisappear ***")
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    
    @IBAction func selectImageForProfilePicture(_ sender: UITapGestureRecognizer) {
        self.bottomActionSheetPressed()
        print("*** selectImage Tapped ***")
    }
    
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.view.subviews.forEach{ $0.resignFirstResponder() }

        if (self.firstName_textField.text != "" && self.lastName_textField.text != "") {
    self.currentPresenter.processEditProfileInfo(submittedForm: editProfileForm)
            self.submitAlertAction()
            //self.showProfileChangesAlert()
            //self.currentPresenter.wireframe.dismiss(true)
        //self.navigationController?.dismiss(animated: true, completion: nil)
            //self.currentPresenter.wireframe.navigate(to: .ProfileView)
        } else {
            testRequiredFields()
        }
        
        }
    
    private func repopulateProfileInfo(){
        guard let userProfile = self.profile else {
            return
        }
        self.firstName_textField
            .set(text: userProfile.firstname)
        self.lastName_textField
            .set(text: userProfile.lastname)
        self.nickname_label.text = "\(userProfile.firstname)"
        self.mobileNumber_textField
            .set(text: userProfile.mobile)
        self.email_textField
            .set(text: userProfile.email)
    }
    
    //test validations
    func testRequiredFields(){
        
        self.firstName_textField
            .set(required: true)
            .set(regex: [GBAValidations.alphaNumeric.stringValue], validationMessage: ["Invalid first name"])
        
        self.lastName_textField
            .set(required: true)
            .set(regex: [GBAValidations.alphaNumeric.stringValue], validationMessage: ["Invalid last name"])
        
    }
    
    func showProfileChangesAlert() {
        let alertView = UIAlertController(title: "Profile Updated",
                                          message: "The following changes will take effect once you relog-in",
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.transactionSubmitted()}))
//        let okAction = UIAlertAction(title: "Ok", style: .default)
//        alertView.addAction(okAction)
        present(alertView, animated: true)
    }
    
    //MARK: PhotoLibrary Functions
    func selectFromGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: Camera Functions
    func getAnImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraImagePickerController = UIImagePickerController()
            cameraImagePickerController.sourceType = .camera
            cameraImagePickerController.delegate = self
            present(cameraImagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: Bottom ActionSheet
    
    func bottomActionSheetPressed() {
        let alert = UIAlertController(title: nil, message: "Select image for your profile photo", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Select from gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.selectFromGallery()}))
        alert.addAction(UIAlertAction(title: "Capture an image", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.getAnImage()}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Alert for the submitAlertAction()
    func submitAlertAction() {
        let alert = UIAlertController(title: "Update Profile", message: "Do you like the following changes be saved?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.showProfileChangesAlert()}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: PhotoLibrary Function Delelegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {fatalError("Expected a dictionary containing an image, but was provided the following: \(info)") }
        self.profile_imageView.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Segue for the Submit Button
    func transactionSubmitted() {
        self.currentPresenter.wireframe.popToRootViewController(true) //navigate(to: .AccessSettingsMainView)
//        guard let nav = self.navigationController else { fatalError("NavigationViewController can't properly parsed")}
//        nav.dismiss(animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func didChange(value: String?) {
        self.nickname_label.text = value
    }
}


extension EditProfileViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditProfileViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}

extension EditProfileViewController: DataDidReceiveFromEditProfile{
    func didReceiveResponse(code: String) {
        print("Data Received")
    }
}



