//
//  ProfilePresenter.swift
//  GBA
//
//  Created by Gladys Prado on 18/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

struct UserProfile{
    var firstName: String
    var lastName: String
    var nickname: String?
    var mobileNumber: String
    var email: String
}

class ProfilePresenter: SettingsRootPresenter {
    var profile: UserProfile?
}

struct EditProfile: Decodable{
    var firstName: String
    var lastName: String
}
