//
//  SettingsRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 8/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//
import Foundation
import UIKit

fileprivate enum APICalls: Router {
    
    var baseComponent: String { get { return "/public" } }
    
    //Cases
    case changeVerification(authLevel: String)
    case editProfileName(firstname: String, lastname: String)
    case updatePassword(oldpassword: String, newpassword: String, newpasswordconfirm: String)

    
    //Router Setup
    var route: Route {
        
        switch self {
        case .changeVerification(let authLevel):
            return Route(method: .post,
                         suffix: "/verification",
                         parameters: ["auth_level" : authLevel],
                         waitUntilFinished: true,
                         nonToken: true)
            
        case .editProfileName(let firstname, let lastname):
            return Route(method: .post,
                         suffix: "/profile",
                         parameters: ["firstname" : firstname,
                                      "lastname" : lastname],
                         waitUntilFinished: true,
                         nonToken: true)
        case .updatePassword(let oldpassword, let newpassword, let newpasswordconfirm):
            return Route(method: .post,
                         suffix: "/change/password",
                         parameters: ["old_password": oldpassword,
                                      "password": newpassword,
                                      "password_confirm": newpasswordconfirm],
                         waitUntilFinished: true,
                         nonToken: true)
        }
    }
}

class SettingsRemoteInteractor: RootRemoteInteractor{
    
    // Contact Support API Calls
    
    func submitChangeVerificationLvl(form: VerificationLvlEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.changeVerification(authLevel: form.authLevel!),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }
    
    func submitEditProfileInfo(form: EditProfile, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.editProfileName(firstname: form.firstName, lastname: form.lastName),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }
    
    func submitNewPassword(form: UpdatePasswordFormEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.updatePassword(oldpassword: form.oldPassword, newpassword: form.newPassword, newpasswordconfirm: form.newPasswordConfirm),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }

}
