//
//  SettingsRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class SettingsRootPresenter: RootPresenter {
    var wireframe: SettingsWireframe
    var view: SettingsRootViewController
    
    var interactor: (local: SettingsLocalInteractor, remote: SettingsRemoteInteractor) = (SettingsLocalInteractor(), SettingsRemoteInteractor())

    init(wireframe: SettingsWireframe, view: SettingsRootViewController) {
        self.wireframe = wireframe
        self.view = view
    }
    
    func set(view: SettingsRootViewController){
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
    
}

