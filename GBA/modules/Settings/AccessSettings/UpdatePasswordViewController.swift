//
//  UpdatePasswordViewController.swift
//  GBA
//
//  Created by Gladys Prado on 12/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class UpdatePasswordViewController: SettingsRootViewController, GBAFocusableInputViewDelegate {
    
    @IBOutlet weak var txtOldPassword: GBATitledTextField!
    @IBOutlet weak var txtNewPassword: GBATitledTextField!
    @IBOutlet weak var txtConfirmPassword: GBATitledTextField!
    
    var passwordUpdateForm: UpdatePasswordFormEntity{
        return UpdatePasswordFormEntity(oldPassword: self.txtOldPassword.text, newPassword: self.txtNewPassword.text, newPasswordConfirm: self.txtConfirmPassword.text)
    }
    
    var currentPresenter: UpdatePasswordPresenter{
        guard let prsntr = self.presenter as? UpdatePasswordPresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.set(view: self)
        (self._presenter as! UpdatePasswordPresenter).dataBridge = self
        
        self.txtOldPassword
            .set(self)
            .set(placeholder: "Old Password")
            .set(returnKey: .next)
            .set(security: true)
            .set(next: txtNewPassword)
        
        self.txtNewPassword
            .set(self)
            .set(placeholder: "New Password")
            .set(returnKey: .next)
            .set(security: true)
            .set(next: txtConfirmPassword)
        
        self.txtConfirmPassword
            .set(self)
            .set(placeholder: "Confirm Password")
            .set(security: true)
            .set(returnKey: .done)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Update Password"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    //standard submission
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        // txtOldPassword , txtNewPassword , txtConfirmPassword
        if (self.txtOldPassword.text != "" && self.txtNewPassword.text != "" && self.txtConfirmPassword.text != "" && self.txtNewPassword.text == self.txtConfirmPassword.text) {
            self.submitAlertAction()
        } else {
            print("testRequiredFields")
            self.testRequiredFields()
        }
    }
    
    
    // Alert for the submitAlertAction()
    func submitAlertAction() {
        let alert = UIAlertController(title: "Update Profile", message: "Do you like the following changes be saved?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.transactionSubmitted()}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Segue for the Submit Button
    func transactionSubmitted() {
        print(" ****** transactionSubmitted ****** ")
           self.currentPresenter.processNewPassword(submittedForm: passwordUpdateForm)
    }
    
    //test validations
    func testRequiredFields(){
        
        self.txtOldPassword
            .set(required: true)
            .set(regex: [GBAValidations.passwordRegex.stringValue], validationMessage: ["Please re-enter current password"])
        
        self.txtNewPassword
            .set(required: true)
            .set(regex: [GBAValidations.passwordRegex.stringValue], validationMessage: ["Password must be more than 8 characters, with at least one capital, numeric and special character"])
        
        self.txtConfirmPassword
            .set(required: true)
            .set(equalTo: self.txtNewPassword.text, validationMessage: ["Passwords do not match"])
    }
    
}

extension UpdatePasswordViewController: DataDidReceiveFromUpdatePassword{
    func didReceiveNewPassword(code: String) {
        print("Password Updated!")
    }
}

