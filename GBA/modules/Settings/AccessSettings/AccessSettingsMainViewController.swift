//
//  AccessSettingsMainViewController.swift
//  GBA
//
//  Created by Gladys Prado on 12/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

struct VerificationLvlEntity: Decodable{
    var authLevel: String? = ""
}

struct LevelType {
    private(set) public var type: String
    init(type: String) {
        self.type = type
    }
}

enum VerificationType {
    case OneWay
    case TwoWay
    
    var type: String{
        switch self{
        case .OneWay: return "1"
        case .TwoWay: return "2"
        }
    }
    
}

class AccessSettingsMainViewController: SettingsRootViewController {
    
    @IBOutlet weak var viewTouchIDHolder: UIView!
    @IBOutlet weak var viewPINCodeHolder: UIView!
    @IBOutlet weak var viewVerificationCodeHolder: UIView!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnChangeAccesSettingPINCode: UIButton!
    
    //switches
    @IBOutlet weak var switchTouchID: UISwitch!
    @IBOutlet weak var switchPINCode: UISwitch!
    @IBOutlet weak var switchVerificationCode: UISwitch!
    
    
    //EXP.
    fileprivate var user: User{
        get{
            guard let usr = GBARealm.objects(User.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    var selectedVerificationLvl: String = "0"
    var selectedVerificationType: VerificationType = .OneWay
    
    var verificationForm: VerificationLvlEntity{
        return VerificationLvlEntity(authLevel: self.selectedVerificationType.type)
    }
    
    var currentPresenter: AccessSettingsPresenter{
        guard let prsntr = self.presenter as? AccessSettingsPresenter
            else{ fatalError("Error in parsing presenter for AccessSettingsPresenter") }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.presenter.set(view: self)
        (self._presenter as! AccessSettingsPresenter).dataBridge = self
        
        viewTouchIDHolder.applyCornerRadius(10)
        viewPINCodeHolder.applyCornerRadius(10)
        viewVerificationCodeHolder.applyCornerRadius(10)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.title = "Access Settings"
        repopulateProfileInfo()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc override func backBtn_tapped(){
//        guard let nav = self.navigationController else { fatalError("NavigationViewController can't properly parsed")}
//        nav.dismiss(animated: true, completion: nil)
        //self.currentPresenter.wireframe.dismiss(true)
        self.currentPresenter.wireframe.popToRootViewController(true)
    }
    
    
    @IBAction func didTapChangePinCode(_ sender: Any) {
        guard let navController = self.navigationController else { return }
        SettingsWireframe(navController).oldPinCodeVC()
//        let vc = ChangePinCodeViewController()
//        self.navigator.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapChangePassword(_ sender: Any) {
        self.presenter.wireframe.navigate(to: .UpdatePasswordView)
    }

    @IBAction func didTapVerificationCode(_ sender: UISwitch) {
        if switchVerificationCode.isOn {
            print("switchVerificationCode.isOn")
            self.selectedVerificationLvl = "2"
            self.selectedVerificationType = VerificationType.TwoWay
            
            //let selectedVerification = self.selectedVerificationType
            self.showSettingChangesAlert()
            //self.currentPresenter.processVerificationLvl(submittedForm: verificationForm)
            print(verificationForm)
        } else {
            print("switchVerificationCode.isOff")
            self.selectedVerificationLvl = "1"
            self.selectedVerificationType = VerificationType.OneWay
            self.showSettingChangesAlert()
            //self.currentPresenter.processVerificationLvl(submittedForm: verificationForm)
            print(verificationForm)
        }
    }
    
    // Alert for the AlertAction()
    func showSettingChangesAlert() {
        let alertView = UIAlertController(title: "Update Settings",
                                          message: "The following changes has been saved",
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.transactionSubmitted()}))
        present(alertView, animated: true)
    }
    
    private func repopulateProfileInfo(){
        let userProfile = self.user
        if userProfile.auth_level == 2 {
            switchVerificationCode.setOn(true, animated: true)
            print("AuthLevel set to 2")
        } else {
            print("AuthLevel set to 1")
        }
    }
    
    
    // Segue for the Submit Button
    func transactionSubmitted() {
        print(" ****** transactionSubmitted ****** ")
        //self.currentPresenter.processVerificationLvl(submittedForm: verificationForm)

    }

}

extension AccessSettingsMainViewController: DataDidReceiveFromAccessSettings{
    func didReceiveResponse(code: String){
        print("did Receive From Access Settings")
    }
}
