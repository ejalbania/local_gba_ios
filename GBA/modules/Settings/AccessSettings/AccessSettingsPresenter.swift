//
//  AccessSettingsPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 12/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol DataDidReceiveFromAccessSettings{
    func didReceiveResponse(code: String)
}

class AccessSettingsPresenter: SettingsRootPresenter {
    
    var submittedForm: VerificationLvlEntity? = nil
    var dataBridge: DataDidReceiveFromAccessSettings? = nil
    
    func processVerificationLvl(submittedForm: VerificationLvlEntity){
        self.submittedForm = submittedForm
        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in ContactSupportPresenter") }
        print(submittedForm)
        
        self.interactor.remote.submitChangeVerificationLvl(form: submittedForm, successHandler: {
            (reply, statusCode) in
            print(reply)
            print(statusCode)
            
            switch statusCode{
            case .fetchSuccess:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")
                
                /**************************************************************************/
                
            case .notModified:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")
                
                /**************************************************************************/
                
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                self.showAlert(with: "Message", message: message ?? "message not found", completion: { () })
            default: break
            }
        })
    }
}




