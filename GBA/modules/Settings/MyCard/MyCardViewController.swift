//
//  MyCardViewController.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class MyCardViewController: SettingsRootViewController {
    
    @IBOutlet weak var cardImageView_container: GBACard!
    
    fileprivate var profile: User?{
        get{ return self.presenter.interactor.local.userProfile }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.repopulateCardInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.title = "My Card"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = " "
    }
    
    private func repopulateCardInfo(){
        guard let userProfile = self.profile else{
                return
        }
        self.cardImageView_container
            .set(owner: userProfile.Fullname)
        
        self.cardImageView_container.layoutSubviews()
    }
}
