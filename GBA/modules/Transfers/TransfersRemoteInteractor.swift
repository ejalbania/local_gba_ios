//
//  TransfersRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 24/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum TransferAPICalls: Router{
    
    var baseComponent: String { get {  return "public" } }
    
    case transferToSomeone(user_wallet_id: String, recipient_id: String, recipient_wallet_id: String, amount: String, date: String, time: String?)
    
    var route: Route {
        switch self {
        case .transferToSomeone(let wallet_id, let recipient_id, let recipient_wallet_id, let amount, let date, let time):
            return Route(method: .post,
                         suffix: "/transactions/transfer",
                         parameters:
                [ "wallet_id"               : wallet_id,
                  "recipient_id"            : recipient_id,
                  "recipient_wallet_id"     : recipient_wallet_id,
                  "amount"                  : amount,
                  "date"                    : date,
                  "time"                    : time!],
                         waitUntilFinished: true)
            
        }
    }
}

class TransfersRemoteInteractor: RootRemoteInteractor{
    private var fieldTransferFormEntity: TransferFormEntity? = nil
    
    func TransferPayToSomeone(form: TransferFormEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        fieldTransferFormEntity = form
        
        NetworkingManager.request(TransferAPICalls.transferToSomeone(user_wallet_id: form.user_wallet_id!,
                                                                     recipient_id: form.recipient_id!,
                                                                     recipient_wallet_id: form.recipient_wallet_id!,
                                                                     amount: form.amount!,
                                                                     date: form.date!,
                                                                     time: form.time), successHandler: {
                                                                        (reply, statusCode) in
                                                                        successHandler(reply, statusCode)
                                                                        
        })
    }
}
