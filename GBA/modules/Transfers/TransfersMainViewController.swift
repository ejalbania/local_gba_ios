//
//  TransfersMainViewController.swift
//  GBA
//
//  Created by Gladys Prado on 22/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class TransfersMainViewController: TransfersRootViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var collectionPendingTransactions: UICollectionView!
    @IBOutlet weak var tblTransferOptions: UITableView!
    @IBOutlet weak var btnSeeAllPendingTransactions: UIButton!
    
    //set cell indentifier
    var cellIdentifier = "TransferType"
    
    var nav: UINavigationController{
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = true
        
        return _nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblTransferOptions.delegate = self
        tblTransferOptions.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Transfers"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //start of collection view configuration
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PendingTransactionsList",
                                                      for: indexPath)
        
        // Configure the cell
        return cell
    }
    //--------end of collection view configuration
    
    //start table view configuration
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReloadOptionTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
        
//        let reloadOptionName = Array(reloadOptions)[indexPath.row].key
//        let reloadOptionImage = Array(reloadOptions)[indexPath.row].value
//
//        cell.optionImage?.image = UIImage(named: reloadOptionImage)
//        cell.optionName?.text = reloadOptionName
//
//        print(reloadOptionImage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let _nav = self.nav
        
        TransfersWireframe(_nav).navigate(to: .PaySomeoneRecipientAccountView)
        
        self.present(_nav, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @IBAction func seeAllPendingTransactions(_ sender: Any) {
        let _nav = self.nav
        
        TransfersWireframe(_nav).navigate(to: .PendingTransactionsView)
        
        self.present(_nav, animated: true, completion: nil)
    }
    
    
}
