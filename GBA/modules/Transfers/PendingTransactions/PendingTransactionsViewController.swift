//
//  PendingTransactionsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 1/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PendingTransactionsViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblPendingTransactionsList: UITableView!
    @IBOutlet weak var selectTransactionListCategory: UISegmentedControl!
    
    //set cell indentifier
    var cellIdentifier = "PendingTransactions"
    
    var nav: UINavigationController{
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = true
        
        return _nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPendingTransactionsList.delegate = self
        tblPendingTransactionsList.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.parent?.title = "Pending Transactions"
        
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //start table view configuration
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReloadOptionTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.wireframe.navigate(to: .PendingTransactionsDetailsView)
    }
    
}
