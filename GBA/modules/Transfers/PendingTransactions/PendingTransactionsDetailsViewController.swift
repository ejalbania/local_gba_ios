//
//  PendingTransactionsDetailsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 15/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PendingTransactionsDetailsViewController: TransfersRootViewController {
    
    @IBOutlet weak var btnCancelTransaction: UIButton!
    
    var nav: UINavigationController{
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = true
        
        return _nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnCancelTransaction.layer.cornerRadius = 5
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Pending Transactions"
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
}
