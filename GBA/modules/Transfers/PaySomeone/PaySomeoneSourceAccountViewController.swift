//
//  PaySomeoneSourceAccountViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneSourceAccountViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblSourceAccount: UITableView!
    
    //set cell indentifier
    var cellIdentifier = "SourceAccount"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSourceAccount.delegate = self
        tblSourceAccount.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .PaySomeoneAmountView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DestinationAccountTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
        
        
//        let reloadOptionName = Array(reloadOptions)[indexPath.row].key
//        let reloadOptionImage = Array(reloadOptions)[indexPath.row].value
//
//        cell.optionImage?.image = UIImage(named: reloadOptionImage)
//        cell.optionName?.text = reloadOptionName
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        if (indexPath.row == 0) {
            TransfersWireframe(nav).navigate(to: .PaySomeoneAmountView)
        } else {
            TransfersWireframe(nav).navigate(to: .PaySomeoneAmountView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
