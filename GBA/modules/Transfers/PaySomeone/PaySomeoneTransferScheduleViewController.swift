//
//  PaySomeoneTransferScheduleViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneTransferScheduleViewController: TransfersRootViewController {
    
    @IBOutlet weak var dateTransfer: UIDatePicker!
    @IBOutlet weak var lblDateSelected: UILabel!
    @IBOutlet weak var lblTimeSelected: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        dateTransfer.addTarget(self, action: #selector(datePickerChanged), for: UIControlEvents.valueChanged)
        dateTransfer.setValue(GBAColor.primaryBlueGreen.rawValue, forKey: "textColor")
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm a"
        timeFormatter.amSymbol = "AM"
        timeFormatter.pmSymbol = "PM"
        
        let strDate = dateFormatter.string(from: currentDate)
        let strTime = timeFormatter.string(from: currentDate)
        self.lblDateSelected.text = strDate
        self.lblTimeSelected.text = strTime
        
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .PaySomeoneTransactionSummaryView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func datePickerChanged(dateTransfer: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm a"
        timeFormatter.amSymbol = "AM"
        timeFormatter.pmSymbol = "PM"
        
        let strDate = dateFormatter.string(from: dateTransfer.date)
        let strTime = timeFormatter.string(from: dateTransfer.date)
        self.lblDateSelected.text = strDate
        self.lblTimeSelected.text = strTime
    }
}
