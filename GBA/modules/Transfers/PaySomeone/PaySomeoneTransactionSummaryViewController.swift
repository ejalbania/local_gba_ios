//
//  PaySomeoneTransactionSummaryViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneTransactionSummaryViewController: TransfersRootViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.parent?.title = "Transaction Summary"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempSubmitButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @objc func tempSubmitButton(){
        let amount = NSAttributedString(string: "$100.00",
                                      attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        let message = NSAttributedString(string: " was successfully transferred to ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
        let recipient = NSAttributedString(string: "Johnny",
                                       attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        
        let content = NSMutableAttributedString()
        content.append(amount)
        content.append(message)
        content.append(recipient)
        
        self.presenter.wireframe.presentSuccessPage(title: "Pay Someone", message: content, doneAction: {
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
    }
    
}
