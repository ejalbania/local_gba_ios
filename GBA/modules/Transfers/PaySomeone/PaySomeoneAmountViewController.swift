//
//  PaySomeoneAmountViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneAmountViewController: TransfersRootViewController {
    @IBOutlet weak var txtAmountDeposited: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .PaySomeoneTransferScheduleView)
    }
}
