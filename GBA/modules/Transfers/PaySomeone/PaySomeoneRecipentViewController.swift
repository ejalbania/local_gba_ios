//
//  PaySomeoneRecipentViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneRecipientViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblRecipientAccount: UITableView!
    @IBOutlet weak var btnAddNewRecipient: UIButton!
    
    //set cell indentifier
    var cellIdentifier = "RecipientAccount"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblRecipientAccount.delegate = self
        tblRecipientAccount.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .PaySomeoneSourceAccountView)
    }
    
    @objc override func tempCancelButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .TransfersMainView)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DestinationAccountTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
    

//        let reloadOptionName = Array(reloadOptions)[indexPath.row].key
//        let reloadOptionImage = Array(reloadOptions)[indexPath.row].value
//
//        cell.optionImage?.image = UIImage(named: reloadOptionImage)
//        cell.optionName?.text = reloadOptionName


        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        if (indexPath.row == 0) {
            TransfersWireframe(nav).navigate(to: .PaySomeoneSourceAccountView)
        } else {
            TransfersWireframe(nav).navigate(to: .PaySomeoneSourceAccountView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
}
