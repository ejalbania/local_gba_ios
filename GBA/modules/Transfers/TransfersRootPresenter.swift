//
//  TransfersRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class TransfersRootPresenter: RootPresenter{
    var wireframe: TransfersWireframe
    
    var interactor: (local: TransfersLocalInteractor, remote: TransfersRemoteInteractor) = (TransfersLocalInteractor(), TransfersRemoteInteractor())
    var view: TransfersRootViewController
    
    init(wireframe: TransfersWireframe, view: TransfersRootViewController){
        self.wireframe = wireframe
        self.view = view
    }
}
