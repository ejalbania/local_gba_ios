//
//  TransactionHistoryRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 26/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "public" } }
    
    case getAllTransactionHistories
    
    var route: Route{
        switch self {
        case .getAllTransactionHistories:
            return Route(method: .get,
                         suffix: "/transactions?limit=10&page=1",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        }
    }
}

class TransactionHistoryRemoteInteractor: RootRemoteInteractor{
    
    func getAllTransactionHistories(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.getAllTransactionHistories, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
}
