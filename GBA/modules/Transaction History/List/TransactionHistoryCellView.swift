//
//  TransactionHistoryCellView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class TransactionHistoryCellView: UITableViewCell {

    @IBOutlet weak var caret_label: UILabel!
    
    fileprivate(set) var parent: RootViewController? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        caret_label.text = String.fontAwesomeIcon(name: .angleRight)
        caret_label.font = UIFont.fontAwesome(ofSize: GBAText.Size.subContent.rawValue)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

extension TransactionHistoryCellView{
    @discardableResult
    func set(parent: RootViewController)->Self{
        self.parent = parent
        return self
    }
}
