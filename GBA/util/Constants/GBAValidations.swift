//
//  GBAValidations.swift
//  GBA
//
//  Created by Gladys Prado on 8/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

enum GBAValidations{
    case emailRegex
    case alphaNumeric
    case passwordRegex
    case mobileNumberMinimumLength
    
    var stringValue: String {
        switch self {
        case .emailRegex:                           return "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        case .alphaNumeric:                         return "^[a-zA-Z0-9- ]*$"
        case .passwordRegex:                        return "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$"
        case .mobileNumberMinimumLength:            return "^.{8,}$"
        }
    }
}
