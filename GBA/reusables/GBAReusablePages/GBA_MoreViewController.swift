//
//  GBA_More.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/27/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import RealmSwift

class GBA_MoreViewController: RootViewController{
    
    var delegate: PinCodeVerificationDelegate? = nil
    
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var transactionHistory_btn: More_optionView!
    @IBOutlet weak var managePayees_btn: More_optionView!
    @IBOutlet weak var customerSupport_btn: More_optionView!
    @IBOutlet weak var settings_btn: More_optionView!
    @IBOutlet weak var logout_btn: More_optionView!
    @IBOutlet weak var btnViewProfile: UIButton!
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userFullName: UILabel!
    
    
    //Realm Q.Variables 
    fileprivate var user: User{
        get{
            guard let usr = GBARealm.objects(User.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    fileprivate var pinUsers: PinUsers{
        get{                //GBARealm.objects(PinUsers.self).first else{ //to be tested
            guard let pinUser = GBARealm.object(ofType: PinUsers.self, forPrimaryKey: "\(user.mobile)") else{
                fatalError("User not found")
            }
            print(user.mobile + "@MoreVC")
            print(pinUser)
            return pinUser
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        self.headerContainerView.apply(gradientLayer: .shadedBlueGreen)
        
        transactionHistory_btn
            .set(title: "Transaction History")
            .set(icon: UIImage(named: "Sidebar_Transaction_History")!)
            .setAction(target: self) {
                TransactionHistoryWireframe(nav).navigate(to: .TransactionHistoryscreen)
                self.present(nav, animated: true, completion: nil)
        }
        
        managePayees_btn
            .set(title: "Manage Payees")
            .set(icon: UIImage(named: "Sidebar_Payees")!)
            .setAction(target: self) {
            ManagePayeesWireframe(nav).navigate(to: .ManagePayeeListscreen)
                self.present(nav, animated: true, completion: nil)
        }
        
        customerSupport_btn
            .set(title: "Customer Support")
            .set(icon: UIImage(named: "Sidebar_Customer_Support")!)
            .setAction(target: self) {
                EntryWireframe(nav).navigate(to: .Supportscreen)
                self.present(nav, animated: true, completion: nil)
        }

        settings_btn
            .set(title: "Settings")
            .set(icon: UIImage(named: "Sidebar_Settings")!)
            .setAction(target: self) {
        
                let pinUserProfile = self.pinUsers
                print(pinUserProfile.userNumber! + "@MoreVC Settings")
                if pinUserProfile.pinNumber == nil {
                        self.pinAlertAction()
                } else if pinUserProfile.pinNumber != nil {
                    guard let navController = self.navigationController else { return }
                    SettingsWireframe(navController).presentPinCodeVC()
                } else {
                SettingsWireframe(nav).navigate(to: .AccessSettingsMainView)
                }
                //previous build
//                SettingsWireframe(nav).presentPinCodeVC()
//                self.present(nav, animated:true, completion: nil)
                
        }
        
        logout_btn
            .set(title: "Logout")
            .set(icon: UIImage(named: "Sidebar_LogOut")!)
            .setAction(target: self) {
                do{
                    try UserAuthentication().truncate()
                    try User().truncate()
                    try Payee().truncate()
                    try Wallet().truncate()
                }catch{ print(error.localizedDescription) }
                
                EntryWireframe(nav).navigate(to: .Loginscreen)
                self.present(nav, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.repopulateProfileInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.view.frame = UIScreen.main.bounds
        self.view.layoutIfNeeded()
    }
    
    @IBAction func didTapProfile(_ sender: Any) {
        guard let navController = self.navigationController else { return }
        navController.viewControllers.last?.title = " "
        SettingsWireframe(navController).navigate(to: .ProfileView)
    }
    
    private func repopulateProfileInfo(){
        let userProfile = self.user
        self.userFullName.text = userProfile.Fullname
    }
    
    
    // Alert for the submitAlertAction()
    func pinAlertAction() {
        let alert = UIAlertController(title: "Set a Security PIN", message: "To provide you better security, we will require you to enter your PIN before changing your Settings.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.transactionSubmitted()}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Segue for the Submit Button
    func transactionSubmitted() {
        print(" ****** transactionSubmitted ****** ")
        guard let navController = self.navigationController else { return }
        SettingsWireframe(navController).presentPinCodeVC()
        //self.present(navController, animated:true, completion: nil)
    }
    
    
    
}
