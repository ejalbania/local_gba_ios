//
//  GBAPopupTableview.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/17/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class GBACountryCellView: UITableViewCell{
    private let _view: UIView = UIView()
    
    var flagImage: UIImageView = UIImageView()
    var countryLabel: UILabel = UILabel()
    var countryCode: String = ""
    fileprivate(set) var country: Countries = .Philippines
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        
        self.addSubview(flagImage)
        self.addSubview(countryLabel)
        self.addSubview(_view)
        
        flagImage.contentMode = .scaleAspectFit
        
        flagImage.translatesAutoresizingMaskIntoConstraints = false
        countryLabel.translatesAutoresizingMaskIntoConstraints = false
        _view.translatesAutoresizingMaskIntoConstraints = false
        
        self._view.topAnchor.constraint(equalTo: self.topAnchor).Enable()
        self._view.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        self._view.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        self._view.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
        
        self.flagImage.topAnchor.constraint(equalTo: self._view.topAnchor).Enable()
        self.flagImage.leadingAnchor.constraint(equalTo: self._view.leadingAnchor, constant: 20).Enable()
        self.flagImage.bottomAnchor.constraint(equalTo: self._view.bottomAnchor).Enable()
        self.flagImage.heightAnchor.constraint(equalTo: self.flagImage.widthAnchor).Enable()
        
        self.countryLabel.topAnchor.constraint(equalTo: self._view.topAnchor).Enable()
        self.countryLabel.leadingAnchor.constraint(equalTo: self.flagImage.trailingAnchor, constant: 20).Enable()
        self.countryLabel.bottomAnchor.constraint(equalTo: self._view.bottomAnchor).Enable()
        self.countryLabel.trailingAnchor.constraint(equalTo: self._view.trailingAnchor).Enable()
    }
}

class GBAPopOverCountryTableview: GBAPopOverRootViewController{
    
    var country: [Countries] = [Countries]()
    override var preferredContentSize: CGSize{
        get{
            let multiplier = self.country.count > 4 ? 4: self.country.count
            return CGSize(width: self.view.width, height: 44 * multiplier.Float)
        }
        set { super.preferredContentSize = newValue }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        country.append(.Philippines)
        country.append(.America)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        let cell = GBACountryCellView()
        
        cell.flagImage.image = UIImage(named: country[index].id)
        cell.countryLabel.text = country[index].name
        cell.countryCode = "+\(country[index].ndd.first!)"
        cell.country = country[index]
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return country.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            guard let del = self.delegate else { fatalError("delegate was not properly set for GBAPopOverContryTableView") }
            
            guard let cellView = tableView.cellForRow(at: indexPath) else { fatalError("cellView can't be found") }
            
            del.GBAPopOver(didSelect: cellView)
        }
    }
}
