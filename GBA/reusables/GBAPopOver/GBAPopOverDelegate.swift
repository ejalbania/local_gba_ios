//
//  GBAPopoverDelegate.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/19/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell)
}
