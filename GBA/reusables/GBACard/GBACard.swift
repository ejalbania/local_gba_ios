//
//  GBACard.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/23/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class GBACard: UIView {
    fileprivate(set) var cardNumber = "1234 5678 9876 5431"
    fileprivate(set) var cvv = "1234"
    fileprivate(set) var expiryDate = "12/19"
    fileprivate(set) var ownerName = ""
    
    private let backgroundImage: UIImageView = UIImageView(image: UIImage(named: "GBACard_image"))
    private let viewContent = UIView()
    private let cardNumber_label = UILabel()
    private let cvv_label = UILabel()
    private let expiryDate_label = UILabel()
    private let ownerName_label = UILabel()
    private let validThru_label = UILabel()
    private let monthYear_label = UILabel()
    
    fileprivate(set) var isAnimating = false{
        didSet{ self.layoutSubviews() }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        UIView.animate(withDuration: 0.5,
                       animations: { self.viewContent.alpha = 0 } )
        !isAnimating ? self.xibSetup(): ()
    }
}


fileprivate extension GBACard{
    func xibSetup(){
        self.subviews.forEach{ $0.removeFromSuperview() }
        self.backgroundColor = .clear
        
        let height = self.frame.height
        let width = self.frame.width
        
        self.backgroundImage.contentMode = .scaleAspectFill
        self.backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        
        viewContent.backgroundColor = .clear
        viewContent.translatesAutoresizingMaskIntoConstraints = false
        
        self.cardNumber_label
            .set(value: self.cardNumber)
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(height * 0.12).rawValue)
            .add(to: viewContent)
        
        self.cvv_label
            .set(value: self.cvv)
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(height * 0.08).rawValue)
            .add(to: viewContent)

        self.ownerName_label
            .set(value: self.ownerName)
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(height * 0.12).rawValue)
            .add(to: viewContent)
        
        self.expiryDate_label
            .set(value: self.expiryDate)
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(height * 0.12).rawValue)
            .add(to: viewContent)
        
        self.validThru_label
            .set(value: "VALID THRU")
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .right)
            .set(fontStyle: GBAText.Font.main(height * 0.04).rawValue)
            .set(lines: 2)
            .add(to: viewContent)
            .adjustsFontSizeToFitWidth = true
        
        self.monthYear_label
            .set(value: "MONTHYEAR")
            .set(color: GBAColor.white.rawValue)
            .set(alignment: .center)
            .set(fontStyle: GBAText.Font.main(height * 0.05).rawValue)
            .add(to: viewContent)
            .adjustsFontSizeToFitWidth = true
        
        self.addSubview(self.backgroundImage)
        self.addSubview(viewContent)
        
        if (height * 3/2) < self.frame.width{
            self.backgroundImage.topAnchor.constraint(equalTo: self.topAnchor).Enable()
            self.backgroundImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
            self.backgroundImage.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 3/2).Enable()
            self.backgroundImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).Enable()
        }else{
            self.backgroundImage.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
            self.backgroundImage.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
            self.backgroundImage.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 2/3).Enable()
            self.backgroundImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).Enable()
        }
        
        self.viewContent.leadingAnchor.constraint(equalTo: self.backgroundImage.leadingAnchor, constant: width * 0.04).Enable()
        self.viewContent.bottomAnchor.constraint(equalTo: self.backgroundImage.bottomAnchor, constant: -height * (1/15)).Enable()
        self.viewContent.trailingAnchor.constraint(equalTo: self.backgroundImage.trailingAnchor, constant: -width * 0.04).Enable()
        self.viewContent.heightAnchor.constraint(equalTo: self.backgroundImage.heightAnchor, multiplier: 0.45).Enable()
        
        self.cardNumber_label.topAnchor.constraint(equalTo: viewContent.topAnchor).Enable()
        self.cardNumber_label.leadingAnchor.constraint(equalTo: viewContent.leadingAnchor).Enable()
        self.cardNumber_label.trailingAnchor.constraint(equalTo: viewContent.trailingAnchor).Enable()
        self.cardNumber_label.heightAnchor.constraint(equalTo: viewContent.heightAnchor, multiplier: 1/3).Enable()
        
        self.cvv_label.topAnchor.constraint(equalTo: self.cardNumber_label.bottomAnchor).Enable()
        self.cvv_label.leadingAnchor.constraint(equalTo: self.cardNumber_label.leadingAnchor).Enable()
        
        self.expiryDate_label.bottomAnchor.constraint(equalTo: self.ownerName_label.topAnchor).Enable()
        self.expiryDate_label.heightAnchor.constraint(equalTo: self.ownerName_label.heightAnchor).Enable()
        self.expiryDate_label.centerXAnchor.constraint(equalTo: self.ownerName_label.centerXAnchor).Enable()
        
        self.ownerName_label.leadingAnchor.constraint(equalTo: viewContent.leadingAnchor).Enable()
        self.ownerName_label.bottomAnchor.constraint(equalTo: viewContent.bottomAnchor).Enable()
        self.ownerName_label.trailingAnchor.constraint(equalTo: viewContent.trailingAnchor).Enable()
        self.ownerName_label.heightAnchor.constraint(equalTo: viewContent.heightAnchor, multiplier: 1/3).Enable()
        
        self.validThru_label.trailingAnchor.constraint(equalTo: self.expiryDate_label.leadingAnchor).Enable()
        self.validThru_label.bottomAnchor.constraint(equalTo: self.ownerName_label.topAnchor).Enable()
        self.validThru_label.topAnchor.constraint(equalTo: self.expiryDate_label.topAnchor, constant: 2).Enable()
        self.validThru_label.widthAnchor.constraint(equalTo: self.validThru_label.heightAnchor).Enable()
        
        self.monthYear_label.bottomAnchor.constraint(equalTo: self.expiryDate_label.topAnchor, constant: 5).Enable()
        self.monthYear_label.widthAnchor.constraint(equalTo: self.expiryDate_label.widthAnchor).Enable()
        self.monthYear_label.centerXAnchor.constraint(equalTo: self.expiryDate_label.centerXAnchor).Enable()
        
        UIView.animate(withDuration: 0.5) { self.viewContent.alpha = 1 }
    }
}

extension GBACard{
    @discardableResult
    func set(isAnimating: Bool)->Self{
        self.isAnimating = isAnimating
        return self
    }
    
    @discardableResult
    func set(owner name: String)->Self{
        self.ownerName = name
        return self
    }
}
