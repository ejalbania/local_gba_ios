//
//  PinAuthentication.swift
//  GBA
//
//  Created by EDI on 2/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import RealmSwift

//Protocols
protocol SaveNewPinCodeUser {
    func addNewUser(userNumber: String)
}


//PinAuthentication.swift

class PinUsers: Object {
    @objc dynamic var userNumber: String?
    @objc dynamic var pinNumber: String?

    override static func primaryKey() -> String {
        return "userNumber"
    }
}

//Second:
class Users: Object {
   @objc dynamic var userNumber: String?
   @objc dynamic var pinNumber: String?
    //var usersList2 = List<Users>() //EXP
    
    //@objc dynamic var id = 0 EXP
    override static func primaryKey() -> String {
        return "userNumber"
    }
}
