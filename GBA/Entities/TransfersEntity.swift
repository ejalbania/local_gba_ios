//
//  TransfersEntity.swift
//  GBA
//
//  Created by Gladys Prado on 24/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct TransferFormEntity: Decodable{
    var user_wallet_id: String? = ""
    var recipient_id: String? = ""
    var recipient_wallet_id: String? = ""
    var amount: String? = ""
    var date: String? = ""
    var time: String? = nil
    
    init(user_wallet_id: String, recipient_id: String, recipient_wallet_id: String, amount: String, date: String, time: String? = nil){
        self.user_wallet_id = user_wallet_id
        self.recipient_id = recipient_id
        self.recipient_wallet_id = recipient_wallet_id
        self.amount = amount
        self.date = date
        self.time = time
    }
}
